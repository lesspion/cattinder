/*jslint browser:true, node: true */
/*jslint indent: 4 */
/*jshint strict: true */
/*global ActiveXObject*/
"use strict";

var ajax = {
    xhr: null,
    res: null,
    getRes: function (json) {
        var j = json || false;
        if (j) {
            return JSON.parse(this.res);
        }
        return this.res;
    },
    empty: function (obj) {
        var i = 0,
            para;
        for (para in obj) {
            if (obj.hasOwnProperty(para)) {
                i += 1;
            }
        }
        if (i === 0) {
            return true;
        }
        return false;
    },
    getXHR: function () {
        if (window.XMLHttpRequest || window.activeXObject) {
            if (window.activeXObject) {
                try {
                    this.xhr = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                    this.xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            } else {
                this.xhr = new XMLHttpRequest();
            }
        } else {
            this.xhr = null;
        }
        return this.xhr;
    },
    get: function (link, param, fct) {
        this.xhr = this.getXHR();
        var para = "?",
            that = this,
            i;
        if (!this.empty(param)) {
            for (i in param) {
                if (param.hasOwnProperty(i)) {
                    para += i + "=" + param[i] + "&";
                }
            }
            para = para.substr(0, para.length - 1);
        } else {
            para = "";
        }
        this.xhr.open('GET', link + para, true);
        this.xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        this.xhr.send(null);
        this.xhr.onreadystatechange = function () {
            if (that.xhr.readyState === 4 && that.xhr.status === 200) {
                that.res = that.xhr.responseText;
                //console.log(xhr.responseText);
                fct();
            }
        };
    },
    post: function (link, param, fct) {
        this.xhr = this.getXHR();
        var para = "?",
            that = this,
            i;
        if (!this.empty(param)) {
            for (i in param) {
                if (param.hasOwnProperty(i)) {
                    para += i + "=" + param[i] + "&";
                }
            }
            para = para.substr(0, para.length - 1);
        } else {
            para = "";
        }
        this.xhr.open('POST', link, true);
        this.xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        this.xhr.send(para);
        this.xhr.onreadystatechange = function () {
            if (that.xhr.readyState === 4 && that.xhr.status === 200) {
                that.res = that.xhr.responseText;
                //console.log(xhr.responseText);
                fct();
            }
        };
    }
};

