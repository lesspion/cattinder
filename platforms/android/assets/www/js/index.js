/*jslint browser:true, node: true */
/*jslint indent: 4 */
/*global ajax*/
/*jshint strict: true */
"use strict";

var app = {
    res: null,
    favorite: JSON.parse(localStorage.getItem('favorite')) || [],
    blackCatList: JSON.parse(localStorage.getItem('blacklist')) || [],
    position: [],
    renderTab: function (tab) {
        var el = tab === this.favorite ? document.getElementById('favoriteList') : document.getElementById('blackList'),
            i;
        el.innerHTML = "";
        for (i = 0; i < tab.length; i += 1) {
            el.innerHTML = el.innerHTML + "<img src='" + tab[i].picUrl + "' class='metro' />";
            el.innerHTML = el.innerHTML + "<p class='dispA'>Nom: " + tab[i].name + "</p>";
            el.innerHTML = el.innerHTML + "<p class='dispA'>Age: " + tab[i].age + "</p>";
        }
    },
    getStorage: function (key) {
        return JSON.parse(localStorage.getItem(key));
    },
    putInStorage: function (key, tab) {
        localStorage.setItem(key, JSON.stringify(tab));
    },
    exist: function (search, tab) {
        var i = 0;
        for (i = 0; i < tab.length; i += 1) {
            if (search.sha1 === tab[i].sha1) {
                return true;
            }
        }
        return false;
    },
    addFavorite: function () {
        var obj = {
            name: document.getElementById('cat').alt,
            picUrl: document.getElementById('cat').src,
            age: document.getElementById('age').innerText.substr(5, document.getElementById('age').innerText.length - 1),
            sha1: document.getElementById('sha').value
        };
        this.favorite.push(obj);
        this.putInStorage('favorite', this.favorite);
        this.display();
    },
    addBlackCatList: function () {
        var obj = {
            name: document.getElementById('cat').alt,
            picUrl: document.getElementById('cat').src,
            age: document.getElementById('age').innerText.substr(5, document.getElementById('age').innerText.length - 1),
            sha1: document.getElementById('sha').value
        };
        this.blackCatList.push(obj);
        this.putInStorage('blacklist', this.blackCatList);
        this.display();
    },
    display: function () {
        var i = 0,
            that = this;
        for (i = 0; i < this.res.length; i += 1) {
            if (!this.exist(this.res[i], this.favorite) && !this.exist(this.res[i], this.blackCatList)) {
                document.getElementById('cat').src = this.res[i].picUrl;
                document.getElementById('cat').alt = this.res[i].name;
                document.getElementById('nom').innerText = "Nom: " + this.res[i].name;
                document.getElementById('age').innerText = "Age: " + this.res[i].age;
                document.getElementById('sha').value = this.res[i].sha1;
                document.getElementById('contentSearch').style.display = "block";
                break;
            }
        }
        if (this.favorite.length > 0 || this.blackCatList.length > 0) {
            if (this.blackCatList.length !== 0) {
                if (document.getElementById('sha').value === this.blackCatList[this.blackCatList.length - 1].sha1) {
                    document.getElementById('contentSearch').style.display = "none";
                    document.getElementById('result').style.display = "block";
                    if (this.favorite.length - 1 + this.blackCatList - 1 !== 22) {
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function (pos) {
                                that.position = [pos.coords.latitude, pos.coords.longitude];
                                ajax.get('http://catinder.samsung-campus.net/proxy.php', {
                                    position: that.position[1] + "," + that.position[0]
                                }, function () {
                                    that.successFetchServer();
                                    document.getElementById('contentSearch').style.display = "block";
                                    document.getElementById('result').style.display = "none";
                                });
                            });
                        } else {
                            ajax.get('http://catinder.samsung-campus.net/proxy.php', {}, function () {
                                that.successFetchServer();
                                document.getElementById('contentSearch').style.display = "block";
                                document.getElementById('result').style.display = "none";
                            });
                        }
                    }
                }
            }
            if (this.favorite.length !== 0) {
                if (document.getElementById('sha').value === this.favorite[this.favorite.length - 1].sha1) {
                    document.getElementById('contentSearch').style.display = "none";
                    document.getElementById('result').style.display = "block";
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (pos) {
                            that.position = [pos.coords.latitude, pos.coords.longitude];
                            ajax.get('http://catinder.samsung-campus.net/proxy.php', {
                                position: that.position[1] + "," + that.position[0]
                            }, function () {
                                that.successFetchServer();
                                document.getElementById('contentSearch').style.display = "block";
                                document.getElementById('result').style.display = "none";
                            });
                        });
                    } else {
                        ajax.get('http://catinder.samsung-campus.net/proxy.php', {}, function () {
                            that.successFetchServer();
                            document.getElementById('contentSearch').style.display = "block";
                            document.getElementById('result').style.display = "none";
                        });
                    }
                }
            }
        }
    },
    successFetchServer: function () {
        this.res = ajax.getRes(true).results;
        this.display();
    },
    init: function () {
        var that = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (pos) {
                that.position = [pos.coords.latitude, pos.coords.longitude];
                ajax.get('http://catinder.samsung-campus.net/proxy.php', {
                    position: that.position[1] + "," + that.position[0]
                }, function () {
                    that.successFetchServer();
                });
            });
        } else {
            ajax.get('http://catinder.samsung-campus.net/proxy.php', {}, function () {
                that.successFetchServer();
            });
        }
    }
};

document.addEventListener('DOMContentLoaded', function () {
    app.init();
    var count = 0,
        ok = false,
        newPos = [],
        posSwipe = [],
        firstSwipe;
    document.getElementById('like').addEventListener('click', function () {
        app.addFavorite();
    });
    document.getElementById('unlike').addEventListener('click', function () {
        app.addBlackCatList();
    });
    document.getElementById('cat').addEventListener('click', function () {
        count += 1;
        if (count === 2) {
            ok = true;
        }
        if (ok) {
            ok = false;
            count = 0;
            app.addFavorite();
        }
        setTimeout(function () {
            ok = false;
            count = 0;
        }, 300);
    });
    document.getElementById('cat').addEventListener('touchmove', function (e) {
        newPos[0] = e.touches[0].pageX - document.body.offsetLeft;
        newPos[1] = e.touches[0].pageY - document.body.offsetTop;
    });
    document.getElementById('cat').addEventListener('touchend', function () {
        if (newPos[0] <= 40 || newPos[0] >= 300) {
            app.addBlackCatList();
        }
    });
    document.getElementById('clean').addEventListener('touchstart', function (e) {
        firstSwipe = e.touches[0].pageX - document.body.offsetLeft;
    });
    document.getElementById('clean').addEventListener('touchmove', function (e) {
        if (e.touches[0].pageX - document.body.offsetLeft <= 30) {
            posSwipe[0] = e.touches[0].pageX - document.body.offsetLeft;
            posSwipe[1] = e.touches[0].pageY - document.body.offsetTop;
        }
    });
    document.getElementById('clean').addEventListener('touchend', function () {
        if (posSwipe[0] >= firstSwipe) {
            document.getElementById('sideMenu').style.display = "block";
        } else {
            document.getElementById('sideMenu').style.display = "none";
        }
    });
    document.getElementById('burger').addEventListener('click', function () {
        document.getElementById('sideMenu').style.display = "block";
    });
    document.getElementById('del').addEventListener('click', function () {
        document.getElementById('sideMenu').style.display = "none";
    });
    document.getElementById('home').addEventListener('click', function () {
        document.getElementById('result').style.display = "none";
        document.getElementById('blackList').style.display = "none";
        document.getElementById('favoriteList').style.display = "none";
        document.getElementById('sideMenu').style.display = "none";
        //document.getElementById('info').innerText = "Cat'inder";
        document.querySelector('.none').style.display = "none";
        document.querySelector('.none').classList.remove('delFav');
        document.querySelector('.none').classList.remove('delBlack');
        document.getElementById('contentSearch').style.display = "block";
    });
    document.getElementById('dispFav').addEventListener('click', function () {
        document.getElementById('contentSearch').style.display = "none";
        document.getElementById('result').style.display = "none";
        document.getElementById('blackList').style.display = "none";
        document.getElementById('sideMenu').style.display = "none";
        //document.getElementById('info').innerText = "Cat'inder - favoris";
        document.querySelector('.none').style.display = "block";
        document.querySelector('.none').classList.add("delFav");
        document.querySelector('.none').classList.remove('delBlack');
        document.getElementById('favoriteList').style.display = "block";
        document.querySelector('.delFav').addEventListener('click', function () {
            localStorage.removeItem('favorite');
            window.location.reload();
        });
        app.renderTab(app.favorite);
    });
    document.getElementById('dispBlack').addEventListener('click', function () {
        document.getElementById('contentSearch').style.display = "none";
        document.getElementById('result').style.display = "none";
        document.getElementById('favoriteList').style.display = "none";
        document.getElementById('sideMenu').style.display = "none";
        //document.getElementById('info').innerText = "Cat'inder - blacklist";
        document.getElementById('blackList').style.display = "block";
        document.querySelector('.none').style.display = "block";
        document.querySelector('.none').classList.remove('delFav');
        document.querySelector('.none').classList.add("delBlack");
        document.querySelector('.delBlack').addEventListener('click', function () {
            localStorage.removeItem('blacklist');
            window.location.reload();
        });
        app.renderTab(app.blackCatList);
    });
    document.addEventListener('offline', function () {
        var el = document.getElementById('msg');
        el.innerHTML = "<h1>Vous êtes actuellement hors-ligne, veuillez vous reconnecter à internet</h1>";
    });
    document.addEventListener('online', function () {
        var el = document.getElementById('msg');
        el.innerHTML = "";
    });
});